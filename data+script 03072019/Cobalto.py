# -*- coding: utf-8 -*-
"""
Created on Thu Aug  8 16:24:09 2019

@author: Otros
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Aug  8 14:02:43 2019

@author: Otros
"""
import numpy as np
import pandas as pd

from PLS import custom_PLS

from sklearn.decomposition import PCA
from sklearn.cross_decomposition import PLSRegression
from sklearn import model_selection
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error, r2_score


data = pd.read_excel('Cobalto.xlsx')
ydata = pd.read_excel('Conc.xlsx')
# Sorting and Deleting duplicates

data.sort_values(by=['Wavelength'], inplace = True)
data.drop_duplicates(inplace = True)
data = data.T
data.columns = data.iloc[0]

ydata.index = ydata['Patron']

#spectra
wl = pd.DataFrame.as_matrix(data[0:1])
x_cal = pd.DataFrame.as_matrix(data.loc[range(1,32)])
x_val = pd.DataFrame.as_matrix(data.loc[range(32,40)])
x_MRC = pd.DataFrame.as_matrix(data.loc[['MR 330 dil', 'MR 825 dil']])

y_cal = pd.DataFrame.as_matrix(ydata.loc[range(1,32),['ppm Co']]).flatten()
y_val = pd.DataFrame.as_matrix(ydata.loc[range(32,40),['ppm Co']]).flatten()

## Plot spectra
#plt.figure(figsize=(8,4.5))
#with plt.style.context(('ggplot')):
#    plt.plot(x_cal.T)
#    plt.xlabel('sensor')
#    plt.ylabel('counts')    
#plt.show()

##rescaling spectra   (not needed due to internal scaling on the PLS main routine)
#scaler = StandardScaler()
#scaler.fit(x_cal)
#x_cal_std = scaler.transform(x_cal)
#x_val_std = scaler.transform(x_val)
#x_MRC_std= scaler.transform(x_MRC)

## Plot scaled spectra
#plt.figure(figsize=(8,4.5))
#with plt.style.context(('ggplot')):
#    plt.plot(x_cal_std.T)
#    plt.xlabel('sensor')
#    plt.ylabel('counts')    
#plt.show()

# RUN PLS and graph nª of components
custom_PLS(x_cal,y_cal,x_val, y_val, plot_components = True)

# force 5 components best performance on prediction.
custom_PLS(x_cal,y_cal,x_val, y_val, force_component = 2)

#pred MRC

plsB = PLSRegression(n_components = 2)
plsB.fit(x_cal,y_cal)
y_MRC_pred = plsB.predict(x_MRC)