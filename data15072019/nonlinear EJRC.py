# -*- coding: utf-8 -*-
"""
Created on Wed Sep 11 20:59:23 2019

@author: Damm
"""
import numpy as np
import scipy as sp
from scipy import stats
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import pandas as pd

def EJCR(x,y):
    ''' Given x and y data, calculates the EJCR for any function two parameter f
    function. 
    Returns a_grid,b_grid,sse,obj_lim   to plot a contour plot at height obj_lim'''   
    def f(x, a, b):
        return a * x + b

    # perform regression
    popt, pcov = curve_fit(f, x, y)
    
    # retrieve parameter values
    a = popt[0]
    b = popt[1]
    
    # calculate parameter stdev
    s_a = np.sqrt(pcov[0,0])
    s_b = np.sqrt(pcov[1,1])
    
    # #####################################
    # Single Variable Confidence Interval
    # #####################################
    # calculate 95% confidence interval
    aL = a-1.96*s_a
    aU = a+1.96*s_a
    bL = b-1.96*s_b
    bU = b+1.96*s_b
    
    # calculate 3sigma interval
    a3L = a-3.0*s_a
    a3U = a+3.0*s_a
    b3L = b-3.0*s_b
    b3U = b+3.0*s_b
    
    # #####################################
    # Nonlinear Joint Confidence Interval
    # #####################################
    # Generate contour plot of SSE ratio vs. Parameters
    # meshgrid is +/- change in the objective value
    m = 200
    
    i1 = np.linspace(a3L,a3U,m)
    i2 = np.linspace(b3L,b3U,m)
    a_grid, b_grid = np.meshgrid(i1, i2)
    n = len(y) # number of data points
    p = 2      # number of parameters
    
    # sum of squared errors
    sse = np.empty((m,m))
    for i in range(m):
        for j in range(m):
            at = a_grid[i,j]
            bt = b_grid[i,j]
            sse[i,j] = np.sum((y-f(x,at,bt))**2)
    
    # normalize to the optimal solution
    best_sse = np.sum((y-f(x,a,b))**2)
    fsse = (sse - best_sse) / best_sse
    
    # compute f-statistic for the f-test
    alpha = 0.05 # alpha, confidence
                 # alpha=0.05 is 95% confidence
#    fstat = sp.stats.f.isf(alpha,p,(n-p))
    fstat = 10000*sp.stats.chi2.isf(alpha,p)
    flim = fstat * p / (n-p)
    obj_lim = flim * best_sse + best_sse
    
    return(a_grid,b_grid,sse,obj_lim)

if __name__ == '__main__':
    #PLS    
    x = y_pred.flatten()
    y = y_val
    a_grid,b_grid,sse,obj_lim = EJCR(x,y)
    #PCR
    x2 = y_pred_PCR
    a_grid2,b_grid2,sse2,obj_lim2 = EJCR(x2,y)
    #MLR
    x3 = y_pred_MLR
    a_grid3,b_grid3,sse3,obj_lim3 = EJCR(x3,y)



    # Create a contour plot
    plt.figure()
    plt.xlabel('Pendiente (a)')
    plt.ylabel('Ordenada al origen (b)')
    #PLS
    CS = plt.contour(a_grid,b_grid,sse,[obj_lim],\
                     colors='blue',linewidths=[1.5])
    plt.clabel(CS, inline=1, fontsize=10, fmt='PLS')
    #PCR
    CS = plt.contour(a_grid2,b_grid2,sse2,[obj_lim2],\
                     colors='green',linewidths=[1.5])
    plt.clabel(CS, inline=1, fontsize=10, fmt='PCR')
    #MLR
    CS = plt.contour(a_grid3,b_grid3,sse3,[obj_lim3],\
                     colors='magenta',linewidths=[1.5])
    plt.clabel(CS, inline=1, fontsize=10, fmt='MLR')
    #center 
    plt.scatter(1,0,s=10,c='red',marker='x')
