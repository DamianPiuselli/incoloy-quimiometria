# -*- coding: utf-8 -*-
"""
Created on Thu Sep 12 10:49:06 2019

@author: Otros
"""

import numpy as np
import pandas as pd

from PLS import custom_PLS

from sklearn.decomposition import PCA
from sklearn.cross_decomposition import PLSRegression
from sklearn import model_selection, linear_model 
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error, r2_score
import matplotlib.pyplot as plt

from EJRC import EJCR

data = pd.read_excel("muestras ICPms vs ICPoes.xlsx")

B_pred_PLS = pd.DataFrame.as_matrix(data["B pred PLS"][5:])
B_pred_PCR = pd.DataFrame.as_matrix(data["B pred PCR"][5:])
B_pred_MLR = pd.DataFrame.as_matrix(data["B pred MLR"][5:])
B_ICPMS = pd.DataFrame.as_matrix(data["B ICPms"][5:])

Co_pred_PLS = pd.DataFrame.as_matrix(data["Co pred PLS"][5:])
Co_pred_PCR = pd.DataFrame.as_matrix(data["Co pred PCR"][5:])
Co_pred_MLR = pd.DataFrame.as_matrix(data["Co pred MLR"][5:])
Co_ICPMS = pd.DataFrame.as_matrix(data["Co ICPms"][5:])


#grafico Boro   
y = B_ICPMS   
 
#PLS
x = B_pred_PLS

a_grid,b_grid,sse,obj_lim = EJCR(x,y)
#PCR
x2 = B_pred_PCR
a_grid2,b_grid2,sse2,obj_lim2 = EJCR(x2,y)
#MLR
x3 = B_pred_MLR
a_grid3,b_grid3,sse3,obj_lim3 = EJCR(x3,y)

##grafico Cobalto  
#y = Co_ICPMS   
# 
##PLS
#x = Co_pred_PLS
#
#a_grid,b_grid,sse,obj_lim = EJCR(x,y)
##PCR
#x2 = Co_pred_PCR
#a_grid2,b_grid2,sse2,obj_lim2 = EJCR(x2,y)
##MLR
#x3 = Co_pred_MLR
#a_grid3,b_grid3,sse3,obj_lim3 = EJCR(x3,y)
    
#    
# #  Create a contour plot
#plt.figure()
##plt.title("Region eliptica de confianza conjunta - Boro")
#plt.xlabel('Slope (a)')
#plt.ylabel('Intercept (b)')
#    #PLS
#CS = plt.contour(a_grid,b_grid,sse,[obj_lim],\
#                     colors='indigo',linewidths=[1])
#plt.clabel(CS, inline=1, fontsize=10, fmt='PLS')
#    #PCR
#CS = plt.contour(a_grid2,b_grid2,sse2,[obj_lim2],\
#                     colors='#dd5d99',linewidths=[1])
#plt.clabel(CS, inline=1, fontsize=10, fmt='PCR')
#    #MLR
#CS = plt.contour(a_grid3,b_grid3,sse3,[obj_lim3],\
#                     colors='#94b4cc',linewidths=[1])
#plt.clabel(CS, inline=1, fontsize=10, fmt='MLR')
#    #center 
#plt.scatter(1,0,s=8,c='blue',marker='x')
#plt.legend()
#plt.savefig("EJCR vs icpms - Boro.jpg", dpi = 600, bbox_inches="tight")


## grafico de linea
#z = np.polyfit(B_ICPMS, B_pred_PLS, 1)
#with plt.style.context(('default')):
#        fig, ax = plt.subplots()
#        ax.scatter(B_pred_PLS,B_ICPMS, c='#94b4cc', edgecolors='#74a6cc')
#        ax.plot(z[1]+z[0]*np.linspace(0.025,0.055,20), np.linspace(0.025,0.055,20), c='#94b4cc', linewidth=1, label='Experimental',linestyle='dotted')
#        ax.plot(np.linspace(0.025,0.055,20),np.linspace(0.025,0.055,20) , c='indigo', linewidth=1, label='Ideal',linestyle='dotted')
#        plt.ylabel('Predicted concentration [mg/L]')
#        plt.xlabel('Measured concentration (ICP-ms) [mg/L]')
##        plt.title("Concentración estimada vs Concentración ICP-ms - Boro")
#        ax.legend()
#        plt.ylim((0.0275,0.051))
#        plt.xlim((0.0275,0.051))
#        plt.savefig("pred vs icpms - boro.jpg", dpi = 600,bbox_inches="tight")
##        
# grafico de linea
z = np.polyfit(Co_ICPMS, Co_pred_PLS, 1)
with plt.style.context(('default')):
        fig, ax = plt.subplots()
        ax.scatter(Co_pred_PLS,Co_ICPMS, c='#94b4cc', edgecolors='#74a6cc')
        ax.plot(z[1]+z[0]*np.linspace(0.14,0.31,20), np.linspace(0.14,0.31,20), c='#94b4cc', linewidth=1, label='Experimental',linestyle='dotted')
        ax.plot(np.linspace(0.14,0.31,20),np.linspace(0.14,0.31,20), c='indigo', linewidth=1, label='Ideal',linestyle='dotted')
        plt.ylabel('Predicted concentration [mg/L]')
        plt.xlabel('Measured concentration (ICP-ms) [mg/L]')
#        plt.title("Concentración estimada vs Concentración ICP-ms - Boro")
        ax.legend()
        plt.ylim((0.14,0.31))
        plt.xlim((0.14,0.31))
        plt.savefig("pred vs icpms - cobalto.jpg", dpi = 600,bbox_inches="tight")
        


