# -*- coding: utf-8 -*-
"""
Created on Mon Sep  2 12:53:39 2019

@author: Otros
"""

import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import numpy as np
import pandas as pd



rB1 = pd.read_excel('espectros.xlsx', sheet_name = 1)
rB2 = pd.read_excel('espectros.xlsx', sheet_name = 2)
rCo1 = pd.read_excel('espectros.xlsx', sheet_name = 3)
rCo2 = pd.read_excel('espectros.xlsx', sheet_name = 4)

#B1 = data[0:12]
#
#B2 = data[42:51]
#
#Co1 = data[55:64]
#
#Co2 = data[85:]
#
#

### plot

#fig,((ax,ax2),(ax3,ax4)) = plt.subplots(2,2, figsize=(8, 8))
fig,(ax,ax2,ax3,ax4) = plt.subplots(1,4, figsize=(12, 4))

linestyle = "solid"
colorB = "#94b4cc"
colorCo = "#94b4cc"
colorNi = "#dd5d99"
colorCr = "indigo"
colorFe = "#dd5d99"

ax.plot(rB1['wavelength'], rB1['B'], linestyle=linestyle, color=colorB)
ax.plot(rB1['wavelength'], rB1['Ni'],linestyle=linestyle, color=colorNi)
ax.plot(rB1['wavelength'], rB1['Cr'],linestyle=linestyle, color=colorCr)
ax.plot(rB1['wavelength'], rB1['B']+rB1['Ni']+rB1['Cr']-1.93*np.min(rB1['Cr']),linestyle="dotted", color="black",linewidth=1, label="Σ")

ax2.plot(rB2['wavelength'], rB2['B'], linestyle=linestyle, color=colorB)
ax2.plot(rB2['wavelength'], rB2['Ni'], linestyle=linestyle, color=colorNi)
ax2.plot(rB2['wavelength'], rB2['Cr'], linestyle=linestyle, color=colorCr)
ax2.plot(rB2['wavelength'], rB2['B']+rB2['Ni']+rB2['Cr']-1.95*np.min(rB2['Cr']),linestyle="dotted", color="black",linewidth=1, label="Σ")

ax3.plot(rCo1['wavelength'], rCo1['Co'], linestyle=linestyle, color=colorCo)
ax3.plot(rCo1['wavelength'], rCo1['Fe'], linestyle=linestyle, color=colorFe)
ax3.plot(rCo1['wavelength'], rCo1['Cr'], linestyle=linestyle, color=colorCr)
ax3.plot(rCo1['wavelength'], rCo1['Co']+rCo1['Fe']+rCo1['Cr']-1.95*np.min(rCo1['Cr']),linestyle="dotted", color="black",linewidth=1, label="Σ")

ax4.plot(rCo2['wavelength'], rCo2['Co'], linestyle=linestyle, color=colorCo)
ax4.plot(rCo2['wavelength'], rCo2['Fe'], linestyle=linestyle, color=colorFe)
ax4.plot(rCo2['wavelength'], rCo2['Cr'], linestyle=linestyle, color=colorCr)
ax4.plot(rCo2['wavelength'], rCo2['Co']+rCo2['Fe']+rCo2['Cr']-1.95*np.min(rCo2['Cr']),linestyle="dotted", color="black",linewidth=1, label="Σ")

ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
ax2.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
ax3.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
ax4.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
#
##lines ax1
#offsetx=0.001
#ax.vlines(x=208.889,ymin=min(B1['Intensity']), ymax=1.1*max(B1['Intensity']), color = 'black',linestyles='dotted')
#ax.text(208.889+offsetx, 1.26*max(B1['Intensity']), "B 208.889 nm" ,color = 'black',rotation=45, fontsize='small', fontweight='semibold')
#
#ax.vlines(x=208.898,ymin=min(B1['Intensity']), ymax=1.1*max(B1['Intensity']), color = 'black',linestyles='dotted')
#ax.text(208.898+offsetx, 1.26*max(B1['Intensity']), "Ni 208.898 nm" ,color = 'black',rotation=45, fontsize='small', fontweight='semibold')
#
#ax.vlines(x=208.912,ymin=min(B1['Intensity']), ymax=1.1 *max(B1['Intensity']), color = 'black',linestyles='dotted')
#ax.text(208.912+offsetx, 1.26*max(B1['Intensity']), "Cr 208.912 nm" ,color = 'black',rotation=45, fontsize='small', fontweight='semibold')
#
#ax.set_ylim(top=2750)
#
##lines ax2
#offsetx=0.001
#ax2.vlines(x=249.772,ymin=min(B2['Intensity']), ymax=1.1*max(B2['Intensity']), color = 'black',linestyles='dotted')
#ax2.text(249.772+offsetx, 1.32*max(B2['Intensity']), "B 249.772 nm" ,color = 'black',rotation=45, fontsize='small', fontweight='semibold')
#
#ax2.vlines(x=249.782,ymin=min(B2['Intensity']), ymax=1.1*max(B2['Intensity']), color = 'black',linestyles='dotted')
#ax2.text(249.782+offsetx, 1.32*max(B2['Intensity']), "Ni 249.782 nm" ,color = 'black',rotation=45, fontsize='small', fontweight='semibold')
#
#ax2.vlines(x=249.787,ymin=min(B2['Intensity']), ymax=1.05 *max(B2['Intensity']), color = 'black',linestyles='dotted')
#ax2.text(249.787+offsetx, 1.26*max(B2['Intensity']), "Cr 249.787 nm" ,color = 'black',rotation=45, fontsize='small', fontweight='semibold')
#
#ax2.set_ylim(top=38000)
#
##lines ax3
#offsetx=0.001
#ax3.vlines(x=228.616,ymin=min(Co1['Intensity']), ymax=1.1*max(Co1['Intensity']), color = 'black',linestyles='dotted')
#ax3.text(228.616+offsetx, 1.27*max(Co1['Intensity']), "Co 228.616 nm" ,color = 'black',rotation=45, fontsize='small', fontweight='semibold')
#
#ax3.vlines(x=228.605,ymin=min(Co1['Intensity']), ymax=1.1*max(Co1['Intensity']), color = 'black',linestyles='dotted')
#ax3.text(228.605+offsetx, 1.27*max(Co1['Intensity']), "Fe 228.605 nm" ,color = 'black',rotation=45, fontsize='small', fontweight='semibold')
#
#ax3.vlines(x=228.631,ymin=min(Co1['Intensity']), ymax=1.1 *max(Co1['Intensity']), color = 'black',linestyles='dotted')
#ax3.text(228.634+offsetx, 1.27*max(Co1['Intensity']), "Cr 228.634 nm" ,color = 'black',rotation=45, fontsize='small', fontweight='semibold')
#
#ax3.set_ylim(top=3600)
#
##lines ax4
#offsetx=0.001
#ax4.vlines(x=236.380,ymin=min(Co2['Intensity']), ymax=1.1*max(Co2['Intensity']), color = 'black',linestyles='dotted')
#ax4.text(236.380+offsetx, 1.27*max(Co2['Intensity']), "Co 236.380 nm" ,color = 'black',rotation=45, fontsize='small', fontweight='semibold')
#
#ax4.vlines(x=236.386,ymin=min(Co2['Intensity']), ymax=1.1*max(Co2['Intensity']), color = 'black',linestyles='dotted')
#ax4.text(236.386+offsetx, 1.27*max(Co2['Intensity']), "Fe 236.386 nm" ,color = 'black',rotation=45, fontsize='small', fontweight='semibold')
#
#ax4.vlines(x=236.402,ymin=min(Co2['Intensity']), ymax=1.1 *max(Co2['Intensity']), color = 'black',linestyles='dotted')
#ax4.text(236.402+offsetx, 1.27*max(Co2['Intensity']), "Cr 236.402 nm" ,color = 'black',rotation=45, fontsize='small', fontweight='semibold')
#
#ax4.set_ylim(top=16000)
#ax4.set_xlim(right=236.428 )
#
ax.set_xlabel('Wavelength [nm]')
ax.set_ylabel('Intensity [cts]')

ax2.set_xlabel('Wavelength [nm]')
ax2.set_ylabel('Intensity [cts]')

ax3.set_xlabel('Wavelength [nm]')
ax3.set_ylabel('Intensity [cts]')

ax4.set_xlabel('Wavelength [nm]')
ax4.set_ylabel('Intensity [cts]')

ax.legend()
ax2.legend()
ax3.legend()
ax4.legend()


plt.tight_layout()
plt.savefig("spectra 4x1.jpg", dpi = 600)
#plt.show()