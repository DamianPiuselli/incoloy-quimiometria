# -*- coding: utf-8 -*-
"""
Created on Thu Jul 25 13:39:47 2019

@author: Otros
"""
from sys import stdout

import numpy as np
import matplotlib.pyplot as plt


from sklearn.decomposition import PCA
from sklearn.cross_decomposition import PLSRegression
from sklearn import model_selection
from sklearn.metrics import mean_squared_error, r2_score




#Define PLS function
def custom_PLS(X_calib, Y_calib, X_valid, Y_valid, plot_components=False, force_component = False):

    #Run PLS including a variable number of components, up to 40,  and calculate MSE
    mse = []
    component_max = 10
    component = np.arange(1, component_max)
    for i in component:
        pls = PLSRegression(n_components=i)
        # Fit
        pls.fit(X_calib, Y_calib)
        # Prediction
        Y_pred = pls.predict(X_valid)
 
        mse_p = mean_squared_error(Y_valid, Y_pred)
        mse.append(mse_p)

        comp = 100*(i+1)/component_max
        # Trick to update status on the same line
        stdout.write("\r%d%% completed" % comp)
        stdout.flush()
    stdout.write("\n")

    # Calculate and print the position of minimum in MSE
    if force_component != False:
        msemin = force_component 
        print("forcing number of components: ", force_component )
        stdout.write("\n")
        
    else:        
        msemin = np.argmin(mse)+1
        print("Suggested number of components: ", msemin)
        stdout.write("\n")
        
        
        

    if plot_components is True:
        with plt.style.context(('ggplot')):
            plt.plot(component, np.array(mse), '-v', color = 'blue', mfc='blue')
            plt.plot(component[msemin-1], np.array(mse)[msemin-1], 'P', ms=10, mfc='red')
            plt.xlabel('Number of PLS components')
            plt.ylabel('MSE')
            plt.title('PLS')
            plt.xlim(xmin=-1)

        plt.show()
        

    # Run PLS with suggested number of components
    pls = PLSRegression(n_components=msemin+1)
    pls.fit(X_calib, Y_calib)
    Y_pred = pls.predict(X_valid) 
    
    # Calculate and print scores
    score_p = r2_score(Y_valid, Y_pred)
    mse_p = mean_squared_error(Y_valid, Y_pred)
    sep = np.std(Y_pred[:,0]-Y_valid)
    rpd = np.std(Y_valid)/sep
    bias = np.mean(Y_pred[:,0]-Y_valid)
    
    print('R2: %5.6f'  % score_p)
    print('MSE: %5.6f' % mse_p) #mean square error
    print('SEP: %5.6f' % sep)  #standard error of prediction
    print('RPD: %5.6f' % rpd)  #
    print('Bias: %5.6f' %  bias)

    # Plot regression and figures of merit
    rangey = max(Y_valid) - min(Y_valid)
    rangex = max(Y_pred) - min(Y_pred)
      

    z = np.polyfit(Y_valid, Y_pred, 1)
    with plt.style.context(('default')):
        fig, ax = plt.subplots()
#        ax.scatter(Y_pred, Y_valid, c='blue', edgecolors='k')
#        ax.plot(z[1]+z[0]*Y_valid, Y_valid, c='magenta', linewidth=1)
#        ax.plot(Y_valid, Y_valid, color='green', linewidth=1)
#        plt.xlabel('Concentracion estimada [mg/L]')
#        plt.ylabel('Concentracion real[mg/L]')
        ax.scatter(Y_valid,Y_pred, c='blue', edgecolors='k')
#        ax.plot(z[1]+z[0]*Y_valid, Y_valid, c='magenta', linewidth=1)
        ax.plot(Y_valid, Y_valid, color='green', linewidth=1)
        plt.ylabel('Concentracion estimada [mg/L]')
        plt.xlabel('Concentracion real [mg/L]')


        # Print the scores on the plot
#        plt.text(min(Y_pred)+0.05*rangex, max(Y_valid)-0.1*rangey, 'R$^{2}=$ %5.6f'  % score_p)
#        plt.text(min(Y_pred)+0.05*rangex, max(Y_valid)-0.15*rangey, 'MSE: %5.6f' % mse_p)
        plt.show()
        

        
        
