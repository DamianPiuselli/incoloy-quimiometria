# -*- coding: utf-8 -*-
"""
Created on Thu Aug  8 16:24:09 2019

@author: Otros
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Aug  8 14:02:43 2019

@author: Otros
"""
import numpy as np
import pandas as pd

from PLS import custom_PLS

from sklearn.decomposition import PCA
from sklearn.cross_decomposition import PLSRegression
from sklearn import model_selection, linear_model 
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error, r2_score


data = pd.read_excel('cobalto.xlsx')
ydata = pd.read_excel('conc.xlsx')
# Sorting and Deleting duplicates

data.sort_values(by=['Wavelength'], inplace = True)
data.drop_duplicates(inplace = True)
data = data.T
data.columns = data.iloc[0]

ydata.index = ydata['Patron']

#spectra
wl = pd.DataFrame.as_matrix(data[0:1])
x_cal = pd.DataFrame.as_matrix(data.loc[range(1,30)])
x_val = pd.DataFrame.as_matrix(data.loc[range(32,48)])
x_MRC = pd.DataFrame.as_matrix(data.loc[['MRC330', 'MRC330.1','MRC825 dilA','MRC825 dilB']])
x_samples =pd.DataFrame.as_matrix(data.loc[['m120',
 'm121',
 'm124',
 'm1301',
 'm1304',
 'm120+ ag1',
 'm121+ ag1',
 'm124+ag1',
 'm1301+ag1',
 'm1304+ag1',
 'm120+ag2',
 'm121+ag2',
 'm124+ag2',
 'm1301+ag2',
 'm1304+ag2',
 'm120+ag3',
 'm121+ag3',
 'm124+ag3',
 'm1301+ag3']])

y_cal = pd.DataFrame.as_matrix(ydata.loc[range(1,30),['ppm Co']]).flatten()
y_val = pd.DataFrame.as_matrix(ydata.loc[range(32,48),['ppm Co']]).flatten()

## Plot spectra
#plt.figure(figsize=(8,4.5))
#with plt.style.context(('ggplot')):
#    plt.plot(x_cal.T)
#    plt.xlabel('sensor')
#    plt.ylabel('counts')    
#plt.show()

##rescaling spectra   (not needed due to internal scaling on the PLS main routine)
#scaler = StandardScaler()
#scaler.fit(x_cal)
#x_cal_std = scaler.transform(x_cal)
#x_val_std = scaler.transform(x_val)
#x_MRC_std= scaler.transform(x_MRC)

## Plot scaled spectra
#plt.figure(figsize=(8,4.5))
#with plt.style.context(('ggplot')):
#    plt.plot(x_cal_std.T)
#    plt.xlabel('sensor')
#    plt.ylabel('counts')    
#plt.show()

## RUN PLS and graph nª of components
#custom_PLS(x_cal,y_cal,x_val, y_val, plot_components = True)
#
## force 4 components, best performance on prediction.
custom_PLS(x_cal,y_cal,x_val, y_val, force_component = 4)

#pred MRC + samples

plsB = PLSRegression(n_components = 4)
plsB.fit(x_cal,y_cal)
y_MRC_pred = plsB.predict(x_MRC)
y_samples_pred = plsB.predict(x_samples)
y_pred = plsB.predict(x_val)

sep = np.std((y_pred-y_val)/y_val)

